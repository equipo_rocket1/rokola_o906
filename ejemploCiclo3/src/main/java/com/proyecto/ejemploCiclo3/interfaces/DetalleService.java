package com.proyecto.ejemploCiclo3.interfaces;

import java.util.List;
import com.proyecto.ejemploCiclo3.modelos.Detalle;

public interface DetalleService {
    public Detalle save(Detalle detalle);
    public void delete(Integer id);
    public Detalle findById(Integer id);
    public List<Detalle> findAll();
}
