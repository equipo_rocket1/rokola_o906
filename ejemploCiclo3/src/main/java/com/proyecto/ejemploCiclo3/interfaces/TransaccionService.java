package com.proyecto.ejemploCiclo3.interfaces;

import java.util.List;
import com.proyecto.ejemploCiclo3.modelos.Transaccion;

public interface TransaccionService {
    public Transaccion save(Transaccion transaccion);
    public void delete(Integer id);
    public Transaccion findById(Integer id);
    public List<Transaccion> findAll();
}
