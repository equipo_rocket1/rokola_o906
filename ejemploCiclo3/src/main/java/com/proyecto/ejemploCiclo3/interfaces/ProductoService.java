package com.proyecto.ejemploCiclo3.interfaces;

import java.util.List;
import com.proyecto.ejemploCiclo3.modelos.Producto;

public interface ProductoService {
    //Puede cambia el nombre de los métodos y agregar más métodos si desea ej: en vez de save puede poner guardar
    //minimo 4 métodos
    public Producto save(Producto producto);
    public void delete(Integer id);
    public Producto findById(Integer id);
    public List<Producto> findAll();
}
