package com.proyecto.ejemploCiclo3.servicios.repositorio;

import org.springframework.data.repository.CrudRepository;
import com.proyecto.ejemploCiclo3.modelos.Producto;

public interface ProductoDAO extends CrudRepository<Producto, Integer>{
    
}
