package com.proyecto.ejemploCiclo3.servicios.repositorio;

import org.springframework.data.repository.CrudRepository;
import com.proyecto.ejemploCiclo3.modelos.Detalle;

public interface DetalleDAO extends CrudRepository<Detalle, Integer>{
    
}
