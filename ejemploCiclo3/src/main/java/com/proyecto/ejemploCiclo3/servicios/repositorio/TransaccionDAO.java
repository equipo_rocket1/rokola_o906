package com.proyecto.ejemploCiclo3.servicios.repositorio;

import org.springframework.data.repository.CrudRepository;
import com.proyecto.ejemploCiclo3.modelos.Transaccion;

public interface TransaccionDAO extends CrudRepository<Transaccion, Integer>{
    
}
