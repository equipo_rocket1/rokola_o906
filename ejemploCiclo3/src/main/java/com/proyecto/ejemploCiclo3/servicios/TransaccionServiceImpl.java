package com.proyecto.ejemploCiclo3.servicios;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.proyecto.ejemploCiclo3.interfaces.TransaccionService;
import com.proyecto.ejemploCiclo3.modelos.Transaccion;
import com.proyecto.ejemploCiclo3.servicios.repositorio.TransaccionDAO;

@Service
public class TransaccionServiceImpl implements TransaccionService{

    @Autowired
    private TransaccionDAO transaccionDao;

    @Override
    @Transactional(readOnly=false)
    public Transaccion save(Transaccion transaccion) {
        return transaccionDao.save(transaccion);
    }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        transaccionDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Transaccion findById(Integer id) {
        return transaccionDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly=true)
    public List<Transaccion> findAll() {
        return (List<Transaccion>) transaccionDao.findAll();
    }   
}