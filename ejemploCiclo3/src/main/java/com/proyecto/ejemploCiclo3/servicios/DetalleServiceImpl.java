package com.proyecto.ejemploCiclo3.servicios;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.proyecto.ejemploCiclo3.interfaces.DetalleService;
import com.proyecto.ejemploCiclo3.modelos.Detalle;
import com.proyecto.ejemploCiclo3.servicios.repositorio.DetalleDAO;

@Service
public class DetalleServiceImpl implements DetalleService{

    @Autowired
    private DetalleDAO detalleDao;

    @Override
    @Transactional(readOnly=false)
    public Detalle save(Detalle detalle) {
        return detalleDao.save(detalle);
    }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        detalleDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Detalle findById(Integer id) {
        return detalleDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly=true)
    public List<Detalle> findAll() {
        return (List<Detalle>) detalleDao.findAll();
    }
}