package com.proyecto.ejemploCiclo3.servicios;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.proyecto.ejemploCiclo3.interfaces.ProductoService;
import com.proyecto.ejemploCiclo3.modelos.Producto;
import com.proyecto.ejemploCiclo3.servicios.repositorio.ProductoDAO;

@Service
public class ProductoServiceImpl implements ProductoService{

    @Autowired
    private ProductoDAO productoDao;

    @Override
    @Transactional(readOnly=false)
    public Producto save(Producto producto) {
        return productoDao.save(producto);
    }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        productoDao.deleteById(id);
        
    }

    @Override
    @Transactional(readOnly=true)
    public Producto findById(Integer id) {
        return productoDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly=true)
    public List<Producto> findAll() {
        return (List<Producto>) productoDao.findAll();
    } 
}