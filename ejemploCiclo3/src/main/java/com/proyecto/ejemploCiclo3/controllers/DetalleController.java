package com.proyecto.ejemploCiclo3.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.proyecto.ejemploCiclo3.interfaces.DetalleService;
import com.proyecto.ejemploCiclo3.modelos.Detalle;

@RestController
@CrossOrigin("*")
@RequestMapping("/detalle")
public class DetalleController {
    
    @Autowired
    private DetalleService detalleservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Detalle> agregar(@RequestBody Detalle detalle){
        Detalle obj = detalleservice.save(detalle);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/list/{id}")
    public ResponseEntity<Detalle> eliminar(@PathVariable Integer id){
        Detalle obj = detalleservice.findById(id);
            if(obj!=null) {
                detalleservice.delete(id);
            }
            else {
                return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
            }
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/list/{id}")
    public ResponseEntity<Detalle> editar(@RequestBody Detalle detalle){
        Detalle obj = detalleservice.findById(detalle.getIdDetalle());
            if(obj!=null) {
                obj.setProducto(detalle.getProducto());
                obj.setTransaccion(detalle.getTransaccion());
                obj.setCantidadDetalle(detalle.getCantidadDetalle());
                obj.setTotalDetalle(detalle.getTotalDetalle());
                obj.setValorDetalle(detalle.getValorDetalle());
                detalleservice.save(obj);
            }
            else {
                return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
            }
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Detalle> consultarTodo(){
        return detalleservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Detalle consultaPorId(@PathVariable Integer id){
        return detalleservice.findById(id);
    }
}