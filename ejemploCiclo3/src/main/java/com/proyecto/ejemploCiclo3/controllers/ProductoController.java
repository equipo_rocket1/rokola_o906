package com.proyecto.ejemploCiclo3.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.proyecto.ejemploCiclo3.interfaces.ProductoService;
import com.proyecto.ejemploCiclo3.modelos.Producto;

@RestController
@CrossOrigin("*")
@RequestMapping("/producto")
public class ProductoController {

    @Autowired
    private ProductoService productoservice;

    @PostMapping(value="/")
    public ResponseEntity<Producto> agregar(@RequestBody Producto producto) {
        Producto obj = productoservice.save(producto);
        System.out.println("add producto controller " + producto.getNombreProducto());
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/list/{id}")
    public ResponseEntity<Producto> eliminar(@PathVariable Integer id) {
        Producto obj = productoservice.findById(id);
            if(obj!=null) {
                productoservice.delete(id);
            }
            else {
                System.out.println("producto no eliminado " + obj);
                return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        System.out.println("producto eliminado " + obj);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //si al hacer put en postman no funciona, cambiarlo por postmapping
    @PutMapping(value="/list/{id}")
    public ResponseEntity<Producto> editar(@RequestBody Producto producto) {
        Producto obj = productoservice.findById(producto.getIdProducto());
        System.out.println("producto id " + obj);
            if(obj!=null) {
                obj.setCantidad(producto.getCantidad());
                obj.setNombreProducto(producto.getNombreProducto());
                obj.setValorCompra(producto.getValorCompra());
                obj.setValorVenta(producto.getValorVenta());
                productoservice.save(obj);
            }
            else {
                System.out.println("producto no update " + obj);
                return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        System.out.println("producto update " + obj);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Producto> consultarTodo() {
        return productoservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Producto consultarPorId(@PathVariable Integer id) {
        System.out.println("consulta id producto controller " + productoservice.findById(id));
        return productoservice.findById(id);
    }
}