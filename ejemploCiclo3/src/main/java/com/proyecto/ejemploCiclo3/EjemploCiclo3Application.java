package com.proyecto.ejemploCiclo3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjemploCiclo3Application {

	public static void main(String[] args) {
		SpringApplication.run(EjemploCiclo3Application.class, args);
	}

}
