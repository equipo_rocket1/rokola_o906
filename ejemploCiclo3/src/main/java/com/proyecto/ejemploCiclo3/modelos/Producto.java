package com.proyecto.ejemploCiclo3.modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="producto")
public class Producto implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idProducto")
    private Integer idProducto;

    @Column(name="nombreProducto")
    private String nombreProducto;

    @Column(name="valorCompra")
    private double valorCompra;

    @Column(name="valorVenta")
    private double valorVenta;

    @Column(name="cantidad")
    private double cantidad;

    public Producto() {
    }

    public Producto(Integer idProducto, String nombreProducto, double valorCompra, double valorVenta, double cantidad) {
        this.idProducto = idProducto;
        this.nombreProducto = nombreProducto;
        this.valorCompra = valorCompra;
        this.valorVenta = valorVenta;
        this.cantidad = cantidad;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public double getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(double valorCompra) {
        this.valorCompra = valorCompra;
    }

    public double getValorVenta() {
        return valorVenta;
    }

    public void setValorVenta(double valorVenta) {
        this.valorVenta = valorVenta;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }
    
}